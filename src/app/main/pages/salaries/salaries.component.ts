import { Component, OnInit, ViewChild, ViewEncapsulation } from "@angular/core";

import { Subject } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import {
  ColumnMode,
  DatatableComponent,
  SelectionType,
} from "@swimlane/ngx-datatable";
import * as snippet from "../datatables.snippetcode";
import { SalariesService } from "app/Services/salaries.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { CoreSidebarService } from "@core/components/core-sidebar/core-sidebar.service";
import { CriteriaEmployee } from "../../../models/CriteriaEmployee";

@Component({
  selector: "app-salaries",
  templateUrl: "./salaries.component.html",
  providers: [],
  styles: [],
})
export class SalariesComponent implements OnInit {
  public contentHeader: object;
  private _unsubscribeAll: Subject<any>;
  private tempData = [];
  public _snippetCodeKitchenSink = snippet.snippetCodeKitchenSink;
  public _snippetCodeInlineEditing = snippet.snippetCodeInlineEditing;
  public _snippetCodeRowDetails = snippet.snippetCodeRowDetails;
  public _snippetCodeCustomCheckbox = snippet.snippetCodeCustomCheckbox;
  public _snippetCodeResponsive = snippet.snippetCodeResponsive;
  public _snippetCodeMultilangual = snippet.snippetCodeMultilangual;
  // public

  public rows: any;
  public selected = [];
  public kitchenSinkRows: any;
  public basicSelectedOption: number = 10;
  public ColumnMode = ColumnMode;
  public expanded = {};
  public editingName = {};
  public editingStatus = {};
  public editingAge = {};
  public editingSalary = {};
  public chkBoxSelected = [];
  public SelectionType = SelectionType;
  public exportCSVData;
  public buttonEnable = true;
  public criteria = CriteriaEmployee;

  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }
  constructor(
    private _service: SalariesService,
    private _coreSidebarService: CoreSidebarService,
    private modalService: NgbModal
  ) {
    //this._coreTranslationService.translate(en, fr, de, pt)
  }
  AppllyFilter(option) {
    const sel = this.selected[0];
    switch (option) {
      case 1:
        // cons la misma officina y grado
        this._service
          .getAllSalaryWithFilter(sel.officeId, sel.grade, sel.positionId)
          .subscribe((data) => {
            this.rows = data;
            this.tempData = this.rows;
            this.kitchenSinkRows = this.rows;
          });
        break;

      case 2:
        this._service
          .getAllSalaryWithFilter(0, sel.grade, 0)
          .subscribe((data) => {
            this.rows = data;
            this.tempData = this.rows;
            this.kitchenSinkRows = this.rows;
          });
        // todas la oficinas y el mismo grado
        break;
      case 3:
        // con la misma posicion y grado
        this._service
          .getAllSalaryWithFilter(0, sel.grade, sel.positionId)
          .subscribe((data) => {
            this.rows = data;
            this.tempData = this.rows;
            this.kitchenSinkRows = this.rows;
          });
        break;
      case 4:
        // todas las posiciones y el mismo grado
        this._service
          .getAllSalaryWithFilter(0, sel.grade, 0)
          .subscribe((data) => {
            this.rows = data;
            this.tempData = this.rows;
            this.kitchenSinkRows = this.rows;
          });
        break;
      default:
        break;
    }
  }
  modalOpenLG(modalLG) {
    this.modalService.open(modalLG, {
      centered: true,
      size: "xl",
    });
  }
  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild("tableRowDetails") tableRowDetails: any;

  rowDetailsToggleExpand(row) {
    this.tableRowDetails.rowDetail.toggleExpandRow(row);
  }
  onActivate(event) {
    // console.log('Activate Event', event);
  }

  onSelect({ selected }) {
    //console.log("Select Event", selected, this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    console.log(this.selected.length);
    if (this.selected.length === 1) {
      this.buttonEnable = false;
    } else {
      this.buttonEnable = true;
    }
  }
  filterUpdate(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.tempData.filter(function (d) {
      return d.fullName.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.kitchenSinkRows = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  ngOnInit(): void {
    this.populateBreadCrumbs();
    this.GetInfo();
    this._service.getMicelaneos();
  }
  public GetInfo() {
    this._service.getAllSalary().subscribe((data) => {
      this.rows = data;
      this.tempData = this.rows;
      this.kitchenSinkRows = this.rows;
    });
  }

  customChkboxOnSelect($event) {}

  private populateBreadCrumbs() {
    this.contentHeader = {
      headerTitle: "V8 Employee  Manager ",
      actionButton: true,
      breadcrumb: {
        type: "",
        links: [
          {
            name: "Nomina",
            isLink: true,
            link: "/",
          },
          {
            name: "Manajo de salarios",
            isLink: false,
          },
        ],
      },
    };
  }
}
