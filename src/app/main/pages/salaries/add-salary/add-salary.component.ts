import { Component, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
// import { SwalComponent } from "@sweetalert2/ngx-sweetalert2";
import { SalariesService } from "app/Services/salaries.service";

@Component({
  selector: "app-add-salary",
  templateUrl: "./add-salary.component.html",
  styleUrls: ["./add-salary.component.scss"],
})
export class AddSalaryComponent implements OnInit {
  constructor(
    private formbuilde: FormBuilder,
    public services: SalariesService
  ) {}

  ngOnInit(): void {}
  public office;
  public msg: string = "";
  // @ViewChild("deleteRecord") private deleteRecord: SwalComponent;

  salaryForm = this.formbuilde.group({
    employeeId: ["", Validators.required],
    beginDate: [""],
    year: ["", Validators.max(new Date().getFullYear())],
    month: [""],
    OfficeId: ["", Validators.required],
    DivisionId: ["", Validators.required],
    PisitionId: [""],
    grade: [""],
    baseSalary: [""],
    productionBonus: [""],
    compensationBase: [""],
    commision: [""],
    constribution: [""],
  });
  submit() {}
  AddToLocal() {
    const year = new Date().getFullYear();
    console.log(year);
    // las validaciones del formulario no se estan disparando
    // mostrar mensajes de los campos obligatorios
    if (this.salaryForm.value.office === "") {
      this.msg = "Debe de selecionar una officina";
      return;
    }
    if (this.salaryForm.value.employeeId === "") {
      //this.deleteRecord.fire();+
      this.msg = "Debe de selecionar un empleado";
      return;
    }
    if (this.salaryForm.value.year > year) {
      this.msg = "El año no puede ser nulo ni mayor al actual";
      return;
    }
    if (this.salaryForm.value.baseSalary === "") {
      this.msg = "Debe proporcionar una salario base";
      return;
    }
    this.msg = "";

    this.salaryForm.value.employeeId = Number(this.salaryForm.value.employeeId);
    this.salaryForm.value.OfficeId = Number(this.salaryForm.value.OfficeId);
    this.salaryForm.value.DivisionId = Number(this.salaryForm.value.DivisionId);
    this.salaryForm.value.PisitionId = Number(this.salaryForm.value.PisitionId);
    this.salaryForm.value.productionBonus = Number(
      this.salaryForm.value.productionBonus
    );
    this.salaryForm.value.compensationBase = Number(
      this.salaryForm.value.compensationBase
    );
    this.salaryForm.value.commision = Number(this.salaryForm.value.commision);
    this.salaryForm.value.constribution = Number(
      this.salaryForm.value.constribution
    );

    this.services.addLocalSalary(this.salaryForm.value);
    this.salaryForm.patchValue({
      employeeId: [this.salaryForm.value.employeeId],
      beginDate: [this.salaryForm.value.beginDate],
      year: [""],
      month: [""],
      OfficeId: [""],
      DivisionId: [""],
      PisitionId: [""],
      grade: [""],
      baseSalary: [""],
      productionBonus: [""],
      compensationBase: [""],
      commision: [""],
      constribution: [""],
    });
  }
}
