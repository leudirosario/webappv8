import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { NgbModule, NgbDropdownToggle } from "@ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";

import { CoreCommonModule } from "@core/common.module";
import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { CardSnippetModule } from "@core/components/card-snippet/card-snippet.module";
import { AuthenticationModule } from "./authentication/authentication.module";
import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { CoreCardSnippetComponent } from "@core/components/card-snippet/card-snippet.component";
import { CoreCardModule } from "@core/components/core-card/core-card.module";
import { CoreSidebarModule } from "@core/components";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CoreCommonModule,
    ContentHeaderModule,
    CoreCardModule,
    NgbModule,
    NgSelectModule,
    FormsModule,
    AuthenticationModule,
    MiscellaneousModule,
    NgxDatatableModule,
    CardSnippetModule,
  ],

  providers: [],
})
export class PagesModule {}
