import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";

import { CoreCommonModule } from "@core/common.module";

import { ContentHeaderModule } from "app/layout/components/content-header/content-header.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { HomeComponent } from "./home.component";
import { SalariesComponent } from "../pages/salaries/salaries.component";
import { CoreCardSnippetComponent } from "@core/components/card-snippet/card-snippet.component";
import { CoreCardModule } from "@core/components/core-card/core-card.module";
import { CardSnippetModule } from "@core/components/card-snippet/card-snippet.module";
import { CoreSidebarModule } from "@core/components";
import { SalaryFormComponent } from "app/components/salary-form/salary-form.component";
import { AddSalaryComponent } from "../pages/salaries/add-salary/add-salary.component";
// import { SweetAlert2Module } from "@sweetalert2/ngx-sweetalert2";
const routes = [
  {
    path: "salaries",
    component: SalariesComponent,
    data: { animation: "sample" },
  },
  {
    path: "home",
    component: HomeComponent,
    data: { animation: "home" },
  },
];

@NgModule({
  declarations: [
    HomeComponent,
    SalariesComponent,
    SalaryFormComponent,
    AddSalaryComponent,
  ],
  imports: [
    RouterModule.forChild(routes),
    ContentHeaderModule,
    TranslateModule,
    CoreCommonModule,
    NgxDatatableModule,
    CoreCardModule,
    CoreSidebarModule,
    CardSnippetModule,
    // SweetAlert2Module,
  ],
  exports: [HomeComponent, SalariesComponent],
})
export class SampleModule {}
