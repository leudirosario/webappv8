import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { environment } from "environments/environment";
import { MicelaneoModel } from "../models/Micelaneo.model";
import { EmployeeModel } from "../models/Employee.model";

@Injectable({
  providedIn: "root",
})
export class SalariesService {
  // get all salaries from api
  public offices: MicelaneoModel[] = [];
  public positions: MicelaneoModel[] = [];
  public divitions: MicelaneoModel[] = [];
  public Employees: EmployeeModel[] = [];
  // temporal table
  public Salaries = [];

  constructor(private _http: HttpClient) {}
  getOffice(id) {
    return this.offices.filter((c) => c.id == id).map((f) => f.name);
  }
  getDivision(id) {
    return this.divitions.filter((c) => c.id == id).map((f) => f.name);
  }
  getPosition(id) {
    return this.positions.filter((c) => c.id == id).map((f) => f.name);
  }

  SendSalariesToServer() {
    const body = JSON.stringify(this.Salaries);
    console.log(body);
    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
    };
    try {
      this._http
        .post(
          `${environment.apiUrl}/api/EmployeeSalaries/AddNewEmployeeSalary`,
          body,
          httpOptions
        )
        .subscribe((response) => {
          console.log(response);
        });
    } catch (error) {
      console.log(error);
    }
  }

  addLocalSalary(Salary) {
    //this.Salaries.push(Salary);
    console.log(Salary);
    this.Salaries.push({ ...Salary });
    console.log(this.Salaries);
  }
  getAllSalaryWithFilter(OfficeId = 0, Grade = 0, PositionID = 0) {
    const _params = new HttpParams();
    _params.set("OfficeID", OfficeId);
    _params.set("Grade", Grade);
    _params.set("PositionID", PositionID);
    //TODO : el params no me funciono hare un querystring
    return this._http.get(
      `${environment.apiUrl}/api/EmployeeSalaries/GetAllEmployeeSalaries?OfficeId=${OfficeId}&Grade=${Grade}&PositionID=${PositionID}`,
      { params: _params }
    );
  }
  getAllSalary() {
    return this._http.get(
      `${environment.apiUrl}/api/EmployeeSalaries/GetAllEmployeeSalaries`
    );
  }
  getMicelaneos() {
    this._http
      .get(`${environment.apiUrl}/api/EmployeeSalaries/GetEmployees`)
      .subscribe((response: EmployeeModel[]) => (this.Employees = response));
    this._http
      .get(`${environment.apiUrl}/api/Micelaneos/Offices`)
      .subscribe((offices: MicelaneoModel[]) => (this.offices = offices));
    this._http
      .get(`${environment.apiUrl}/api/Micelaneos/Positions`)
      .subscribe((position: MicelaneoModel[]) => (this.positions = position));
    this._http
      .get(`${environment.apiUrl}/api/Micelaneos/Divisions`)
      .subscribe((divsion: MicelaneoModel[]) => (this.divitions = divsion));
  }
}
