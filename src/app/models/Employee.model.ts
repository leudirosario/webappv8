export class EmployeeModel {
  id: number;
  employeeName: string;
  employeeCode: string;
  employeeSurName: string;
}
