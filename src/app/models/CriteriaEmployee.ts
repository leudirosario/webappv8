export class CriteriaEmployee {
  constructor(officeid) {
    this.OfficeId = officeid;
  }
  public OfficeId: Number;
  public Grade: Number;
  public PositionId: Number;
}
