import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { CoreSidebarService } from "@core/components/core-sidebar/core-sidebar.service";
import { SalariesService } from "app/Services/salaries.service";

@Component({
  selector: "app-salary-form",
  templateUrl: "./salary-form.component.html",
  styles: [],
})
export class SalaryFormComponent implements OnInit {
  toggleSidebar(name): void {
    this._coreSidebarService.getSidebarRegistry(name).toggleOpen();
  }

  constructor(
    private salaryService: SalariesService,
    private _coreSidebarService: CoreSidebarService,
    private _formBuilder: FormBuilder
  ) {}

  employeeForm = this._formBuilder.group({
    code: [""],
    name: [""],
    surname: [""],
    birthday: [""],
  });

  ngOnInit(): void {
    this.salaryService.getAllSalary();
  }
  submit() {
    // if (form.valid) {
    //   this.toggleSidebar("new-user-sidebar");
    // }
  }
}
