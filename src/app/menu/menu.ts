import { CoreMenu } from "@core/types";

export const menu: CoreMenu[] = [
  {
    id: "home",
    title: "Empleados",
    translate: "MENU.HOME",
    type: "item",
    icon: "home",
    url: "home",
  },
  {
    id: "sample",
    title: "Salarios",
    translate: "MENU.SAMPLE",
    type: "item",
    icon: "dollar-sign",
    url: "salaries",
  },
];
